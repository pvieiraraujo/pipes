import { SettingsService } from './exemplos-pipes/settings.service';
import { BrowserModule } from '@angular/platform-browser';
import { LOCALE_ID, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ExemplosPipesComponent } from './exemplos-pipes/exemplos-pipes.component';
import { CamelCasePipe } from './exemplos-pipes/camel-case.pipe';
import { FiltroArrayPipe } from './exemplos-pipes/filtro-array.pipe';
import { FiltroArrayImpuroPipe } from './exemplos-pipes/filtro-array-impuro.pipe';
@NgModule({
  declarations: [
    AppComponent,
    ExemplosPipesComponent,
    CamelCasePipe,
    FiltroArrayPipe,
    FiltroArrayImpuroPipe,
  ],
  imports: [
    BrowserModule,
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'pt-BR',
    }
    // ettingsService{
    //   provide: LOCALE_ID,
    //   deps: [SettingsService],
    //   useFactory: (settingsService) => settingsService.getLocale(),
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
