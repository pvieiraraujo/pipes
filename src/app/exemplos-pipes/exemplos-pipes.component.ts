import { Component, OnInit } from '@angular/core';
import { interval, Observable } from 'rxjs';

@Component({
  selector: 'app-exemplos-pipes',
  templateUrl: './exemplos-pipes.component.html',
  styleUrls: ['./exemplos-pipes.component.css']
})
export class ExemplosPipesComponent implements OnInit {

  livro: any = {
    titulo: 'A revolução dos bichos: Um conto de fadas',
    rating: 4.867,
    numeroPaginas: 152,
    preco: 29.90,
    dataLancamento: new Date(2007, 1, 10),
    url: 'https://www.amazon.com.br/dp/8535909559/ref=cm_sw_em_r_mt_dp_CR4CFbRC4GT0W'
  };

  livros: string[] = ['Java', 'Angular2'];
  filtro: string;

  titulo:string;


  constructor() { }

  ngOnInit(): void {
  }
  

  addCurso(valor) {
    this.livros.push(valor);
  }

  obterCursos() {
    if (this.livros.length === 0 || this.filtro === undefined
      || this.filtro.trim() === '') {
      return this.livros;
    }

    return this.livros.filter((v) => {
      if (v.toLocaleLowerCase().indexOf(this.filtro.toLocaleLowerCase()) >= 0) {
        return true;
      }
      return false;
    });
  }


  valorAsync = new Promise((resolve, reject) => {
    setTimeout(() => resolve('valor assíncrono'), 2000)
  });

  valorAsync2 = interval(2000).subscribe(valor => console.log('Valor assíncrono2' + valor));

}
